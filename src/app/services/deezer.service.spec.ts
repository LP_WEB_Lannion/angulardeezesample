import { TestBed, inject } from '@angular/core/testing';
import { DeezerService } from './deezer.service';
import { HttpClientModule } from '@angular/common/http';

describe('DeezerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [DeezerService]
    });
  });

  it('should be created', inject([DeezerService], (service: DeezerService) => {
    console.log('toto');
    expect(service).toBeTruthy();
  }));

  it('should return a search', inject([DeezerService], async (service: DeezerService) => {
    const data: any = await service.search('take on me');
    expect(data[0].id).toBe('5b4ef2da188eb40014655a1e');
  }));
});

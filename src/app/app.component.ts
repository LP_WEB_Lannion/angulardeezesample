import { Component } from '@angular/core';
import { DeezerService } from './services/deezer.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  data: any;
  tracklist=[];

  constructor(private dezzer:DeezerService) {
   

  }
  onTrackId(id) {
    console.log("app main" + id);
    this.dezzer.track(id).then((data)=>{
      this.tracklist.push(data);
    })

  }
  title = 'app';
}
